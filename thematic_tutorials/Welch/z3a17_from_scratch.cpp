//TODO Zselenak Florian
#include <iostream>

template <typename ValueType>
class BinTree{


protected:			/*a gyerekosztaly orokolni fogja*/
	class Node{

	private:	
		ValueType value;	/*a template miatt ValueType tipusa lesz, amivel meg lesz hivva*/
		Node *left;
		Node *right;
		int count{0};		/*count 0 kezdoertekkel*/

		//TODO rule of five (ha egy osztalyban van pointer akkor biztos hogy kellesz destruktor)
		Node(const Node &);						/*jobb es balertekek elvileg masolo konstruktor, konstans hogy az ertek maradjon nehogy valtozzon, es refferencia*/
		Node & operator=(const Node &);			/*tiltottuk privatra a masolo konstruktort es masolo ertekadast*/
		Node(Node &&);						/*a &&vel a jobboldalra hivatkozunk: tipus(const - hogy ne valtozzon) &/&& nev*/
		Node & operator=(Node &&);	



	public:
		Node(ValueType value): value(value), left(nullptr), right(nullptr) {}  /*a valuejet feltoltom a value tipussal amivel meghivom,a  pointereket nullra allitom*/
		ValueType getValue(){return value;}   /*tipustol fuggoen fv.*/
		Node * leftChild(){return left;}
		Node * rightChild(){return right;}
		void leftChild(Node * node){left = node;}
		void rightChild(Node * node){right = node;}			/*ezek a node viselkedesei, mindegyikre van fuggveny amit megirtunk*/
		int getCount(){return count;}
		void incCount(){++count;}

	};

	Node *root; /*olyan eros aggregacio hogy a gyoker kompozicioba van a faval, lehetne referencia is, de legyen gyengebb es csak pointer*/
	Node *treep;   /*fa pointer*/
	int depth{0};

private:
	    //TODO rule of five (ha egy osztalyban van pointer akkor biztos hogy kellesz destruktor)
		BinTree(const BinTree &);						/*jobb es balertekek elvileg masolo konstruktor, konstans hogy az ertek maradjon nehogy valtozzon, es refferencia*/
		BinTree & operator=(const BinTree &);			/*tiltottuk privatra a masolo konstruktort es masolo ertekadast*/
		BinTree(const BinTree &&);						/*a &&vel a jobboldalra hivatkozunk: tipus(const - hogy ne valtozzon) &/&& nev*/
		BinTree & operator=(const BinTree &&);	

public:
	BinTree(Node *root = nullptr, Node *treep = nullptr):root(root), treep(treep){}/*:inicializalos lista*/	/*peldanyositashoz letrehozzuk vele egyutt a root es treepet nullptr ertekkel*/
	~BinTree(){				/*Destruktor*/

	deltree(root);

	}	

	BinTree & operator<<(ValueType value);  		/*definialnunk kellett az operatort mert nemismerte a shiftelest amikor meghivtuk*/
													/*azert kell ele a BinTree & hogy egy referenciat kapjunk vissza a BinTree osztalyban*/
	void print(){print(root,std::cout);}
	void print(Node * node, std::ostream & os);		/*valami output stream*/
	void deltree(Node * node);

};




/*-------------------------------------------ZLW-------------------------------------------------------------------------*/

template <typename ValueType>
class ZLWTree : public BinTree<ValueType> {			/*Kiterjesztjuk a BinTree osztalyt, ez lesz a mienk. A valuetype altal meghivhato lesz bamilyen tipussal*/

public:
	ZLWTree(): BinTree<ValueType>(new typename BinTree<ValueType>::Node('/')) {	/*:: hatókör operátor....hivjuk az ősosztaly konstruktorat, es annak atadok egy ropteben csinalt rootot, es berakja a rootba a root ertekbe*/
		this->treep = this->root;				/*a this pointer arra szolgal, ha pl publicba hivunk meg egy fvt, ami a private ertekkel dolgozik*/
    }
    ZLWTree & operator<<(ValueType value);
    
    
};	

/*-------------------------------------------------------------------------------------------------------------*/

/*Binfa epites*/
/*kijottunk az osztalybol, ez a fuggveny a shifthez a binfaba*/
template <typename ValueType>
BinTree<ValueType> & BinTree<ValueType>::operator<<(ValueType value)
{
    if(!treep) {						//ha nemletezik
       
        root = treep = new Node(value);			//csinalok egy nodeot a valueval amit a mainbe hivok meg
        
    } else if (treep->getValue() == value) {
        
        treep->incCount();
        
    } else if (treep->getValue() > value) {
        
        if(!treep->leftChild()) {					/*ha nincs baloldali gyermeke*/
            
            treep->leftChild(new Node(value));
            
        } else {
            
            treep = treep->leftChild();
            *this << value;						/*ujrahivom rekurzivan *ERRE ugymondd*/
        }
        
    } else if (treep->getValue() < value) {
        
        if(!treep->rightChild()) {					/*ha nincs baloldali gyermeke*/
            
            treep->rightChild(new Node(value));
            
        } else {
            
            treep = treep->rightChild();
            *this << value;						/*ujrahivom rekurzivan *ERRE ugymondd*/
        }
        
    }
    
    treep = root;
    
    return *this;						/*vissza kell adni egy referenciat az objektumra, az aktualis fara.*/
}


/*ZLW fa epites*/		/*kijottunk az osztalybol, ez a fuggveny a shifthez a binfaba*/
template <typename ValueType>
ZLWTree<ValueType> & ZLWTree<ValueType>::operator<<(ValueType value)
{
    
    if(value == '0') {
        
        if(!this->treep->leftChild()) {
            
            typename BinTree<ValueType>::Node * node = new typename BinTree<ValueType>::Node(value);
            this->treep->leftChild(node);
            this->treep = this->root;
            
        } else {
            
            this->treep = this->treep->leftChild(); 
        }
        
    } else {

        if(!this->treep->rightChild()) {
            
            typename BinTree<ValueType>::Node * node = new typename BinTree<ValueType>::Node(value);
            this->treep->rightChild(node);
            this->treep = this->root;
            
        } else {
            
            this->treep = this->treep->rightChild(); 
        }
        
    }
    
    return *this;
}


template <typename ValueType>
void BinTree<ValueType>::print(Node *node, std::ostream & os) 
{
    if(node)
    {
        ++depth;
        print(node->leftChild(), os);
        
        for(int i{0}; i<depth; ++i)
            os << "---";            
        os << node->getValue() << " " << depth << " " << node->getCount() << std::endl;     
        
        print(node->rightChild(), os);
        --depth;
    }
    
}


template <typename ValueType>
void BinTree<ValueType>::deltree(Node *node) 
{
    if(node)
    {
        deltree(node->leftChild());
        deltree(node->rightChild());
        
        delete node;
    }
    
}

	

int main(int argc, char** argv, char ** env)		/*char **env kornyezeti valtozok*/
{
    BinTree<int> bt;
    
    bt << 8 << 9 << 5 << 2 << 7;
    
    bt.print();
    
    std::cout << std::endl;  /*<< bele shiftelunk, definialtuk az operatort mert nemismerte nemtom miert, fent a binfa publicba*/

    ZLWTree<char> zt;

    zt <<'0'<<'1'<<'1'<<'1'<<'1'<<'0'<<'0'<<'1'<<'0'<<'0'<<'1'<<'0'<<'0'<<'1'<<'0'<<'0'<<'0'<<'1'<<'1'<<'1';
    
    zt.print();

}
