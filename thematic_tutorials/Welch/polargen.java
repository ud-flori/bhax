//A program forrása: Bátfai Norbert, Juhász István: Javát tanítok könyv 57. o. 

public class Random {
	
	boolean exist = true;
	double value;

	public Random() {

		exist = true;

	}

	public double get() {

		if(exist) {

			double u1,u2,v1,v2,w;
			do {
					u1 = Math.random();
					u2 = Math.random();

					v1 = 2*u1 - 1;
					v2 = 2*u2 - 1;

					w = v1*v1 + v2*v2;
					}

				while(w>1);

				double r = Math.sqrt((-2*Math.log(w)/w));

				value = r*v2;
				exist = !exist;
			
				return r*v1;

				} else {
						exist= !exist;
						return value;
				}
			}
	
			public static void main(String[] args) {

				Random g = new Random();

				for(int i=0;i<10;++i)
					System.out.println(g.get());

			}
}

